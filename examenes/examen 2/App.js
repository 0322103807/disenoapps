  // App.js
  import React, { useState, useEffect } from 'react';
  import { TouchableOpacity, View, Button, ActivityIndicator, SafeAreaView, Platform, StyleSheet, Image, Text } from 'react-native';
  import { fetchTodos } from './api';
  import TodoList from './TodoList';

  const App = () => {
    const [isLoading, setLoading] = useState(true);
    const [todos, setTodos] = useState([]);
    const [filter, setFilter] = useState(null);

    useEffect(() => {
      fetchTodos()
        .then(setTodos)
        .catch(console.error)
        .finally(() => setLoading(false));
    }, []);

    const filteredTodos = todos.filter((todo) => {
      if (filter === 'completed') return todo.completed;
      if (filter === 'not-completed') return !todo.completed;
      return true;
    }).map((todo) => {
      switch (filter) {
        case 'ids':
          return { id: todo.id };
        case 'ids-titles':
          return { id: todo.id, title: todo.title };
        case 'ids-userids':
          return { id: todo.id, userId: todo.userId };
        case 'resolved-ids-userids':
          return todo.completed ? { id: todo.id, userId: todo.userId } : null;
        case 'unresolved-ids-userids':
          return !todo.completed ? { id: todo.id, userId: todo.userId } : null;
        default:
          return todo;
      }
    }).filter(Boolean);

    if (isLoading) return <ActivityIndicator />;

    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Image
              source={require('./assets/nfl-logo.png')} 
              style={styles.logo}
              resizeMode="contain" 
            />
            <Text style={styles.title}>NFL</Text>
          </View>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids')}>
            <Text style={styles.buttonText}>Todos los pendientes (solo IDs)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids-titles')}>
            <Text style={styles.buttonText}>Todos los pendientes (IDs y Títulos)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('not-completed')}>
            <Text style={styles.buttonText}>Pendientes sin resolver (ID y Título)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('completed')}>
            <Text style={styles.buttonText}>Pendientes resueltos (ID y Título)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids-userids')}>
            <Text style={styles.buttonText}>Todos los pendientes (IDs y UserID)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('resolved-ids-userids')}>
            <Text style={styles.buttonText}>Pendientes resueltos (IDs y UserID)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('unresolved-ids-userids')}>
            <Text style={styles.buttonText}>Pendientes sin resolver (IDs y UserID)</Text>
          </TouchableOpacity>
        <TodoList todos={filteredTodos} />
      </View>
      </SafeAreaView>
    );
  };
  const styles = StyleSheet.create({
    button: {
      marginVertical: 5,
      paddingHorizontal: 8,
      paddingVertical: 1,
      borderWidth: 1,
      borderColor: '#D50A0A', 
      borderRadius: 4,
      backgroundColor: '#D50A0A', 
      alignItems: 'center', 
    },
    buttonText: {
      color: '#FFFFFF', 
      fontSize: 16,
    },
    safeArea: {
      flex: 1,
      backgroundColor: '#0A2342', 
    },
    container: {
      flex: 1,
      padding: 20,
      backgroundColor: '#0A2342', 
    },
    header: {
      alignItems: 'center',
      marginBottom: 20,
    },
    logo: {
      width: 120,
      height: 60, 
    },
    title: {
      fontSize: 24,
      fontWeight: 'bold',
      marginTop: 10,
      color: '#FFFFFF', 
    },
  });


  export default App;
