//imports
import React from "react";
import { View, Text, StyleSheet } from 'react-native';

export default function StackScreen() {
  return (
    <View> 
      <Text style={styles.text}>Hola StackScreen</Text>
    </View>
  );
}

// Estilo
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 30
  }
});
